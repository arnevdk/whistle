import random
from copy import copy

import minizinc
import numpy as np

ALL_CARDS = [i + 1 for i in range(52)]

model = minizinc.Model("whist.mzn")
solver = minizinc.Solver.lookup("chuffed")

hand = {5, 20, 34, 21, 7, 51, 2, 13, 11, 12, 25, 27, 33}
trump_card = 15
starting_player = 1

played = []

for trick in range(1,len(hand)+1):
    for card in hand:
        if card in played:
            continue
        extended_model = copy(model)
        extended_model.add_string(f'constraint tricks[1,{trick}] = {card};')
        inst = minizinc.Instance(solver, extended_model)
        inst['Hand'] = hand
        inst['trump_card'] = trump_card
        inst['starting_player'] = starting_player
        inst['current_trick'] = trick
        result = inst.solve()
        print(result)

    played_card = None
    while not played_card or played_card in played:
        played_card = random.choice(hand)
    played.append(played_card)
    model.add_string(f'constraint tricks[1,{trick}] = {played_card};')
